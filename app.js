const dbConnection = require('./connection')
module.exports = app => {
    const connection = dbConnection

    app.post('/register', (req, res) => {
        
        const {
            cedula,
            apellido,
            nombre, 
            direccion,
            telefono
        } = req.body
        
        connection.query('INSERT INTO usuario SET ?', {
            cedula:cedula,apellido:apellido,nombre:nombre,direccion:direccion,telefono:telefono
        }, (err, result) => {
            res.redirect('/login')
        })
    })
}
